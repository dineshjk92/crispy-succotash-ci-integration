
import CommonPage from "./CommonPage";

const header = 'header > h1'
const bookmarkButton = "a[href$='/toggle-bookmark/'] > span"

class BookmarkedFeedsPage extends CommonPage {

    getHeaderText() {
        return cy.get(header).text()
    }

    getBookmarkButton() {
        return cy.get(bookmarkButton)
    }

    clickBookmarkButton() {
        cy.get(bookmarkButton).click()
    }

}
export default BookmarkedFeedsPage;
