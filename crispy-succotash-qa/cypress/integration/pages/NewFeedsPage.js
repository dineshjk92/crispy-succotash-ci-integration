
import CommonPage from "./CommonPage";

const header = 'div.panel-heading h3'
const newFeedTextBox = '#id_feed_url'
const submitButton = '#submit-id-submit'

class NewFeedsPage extends CommonPage {

    getHeaderText() {
        return cy.get(header).text()
    }

    addNewFeedUrl(url) {
        cy.get(newFeedTextBox).type(url)
    }

    clickSubmit() {
        cy.get(submitButton).click()
    }

}
export default NewFeedsPage;
