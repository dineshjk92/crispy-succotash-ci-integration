
import CommonPage from "./CommonPage";

const header = 'header > h1'
const updatesAlert = 'div.alert'
const checkUpdatesButton = "a[href$='/update/']"

class FeedsDetailListPage extends CommonPage {

    getHeaderText() {
        return cy.get(header).text()
    }

    getUpdatesAlert() {
        return cy.get(updatesAlert)
    }

    clickCheckUpdates() {
        cy.get(checkUpdatesButton).click()
    }

}
export default FeedsDetailListPage;