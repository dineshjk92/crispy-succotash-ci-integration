import CommonPage from "./CommonPage";

const loginLink = "a[href$='/login/']";
const signupButton = "a[href$='/register/']";

class MainPage extends CommonPage{
  clickLogin() {
    return cy.get(loginLink).click();
  }

  clickSignup() {
    return cy.get(signupButton).click();
  }
}
export default MainPage;