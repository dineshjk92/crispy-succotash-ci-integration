import CommonPage from "./CommonPage";

const usernameTextBox = "#id_username"
const passwordTextBox = "#id_password"

class LoginPage extends CommonPage{
  login(username, password) {
    if (username !== undefined) {
      cy.get(usernameTextBox).type(username);
    }

    if (password !== undefined) {
      cy.get(passwordTextBox).type(password);
    }

    cy.get('form').submit()
  }
}
export default LoginPage;
