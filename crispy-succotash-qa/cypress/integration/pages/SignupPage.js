import CommonPage from "./CommonPage";

const usernameTextBox = '#id_username'
const passwordTextBox = '#id_password1'
const confirmpasswordTextBox = '#id_password2'
const submitButton = '#submit-id-submit'

class SignupPage extends CommonPage{
    
  signup(username, password) {
    if (username !== undefined) {
      cy.get(usernameTextBox).type(username);
    }
    if (password !== undefined) {
      cy.get(passwordTextBox).type(password);
      cy.get(confirmpasswordTextBox).type(password);
    }
    cy.get(submitButton).click()
  }

}
export default SignupPage;