
When('I request login page', () => {
    cy.request({
        method: "GET",
        url: "/accounts/login/"
    }).as('response')
    cy.get('@response').then((response) => {
        CSRFTokenExtractor.storeCSRFToken(response)
    })
})

When('I make a call to all feeds list', () => {
    cy.request({
        method: "GET",
        url: "/feeds/"
    }).as('response')
})

When('I login via api', (datatable) => {
    datatable.hashes().forEach((element) => {
        cy.get("@csrfToken").then((token) => {
            cy.request({
                method: "POST",
                url: "/accounts/login/",
                form: true,
                body: {
                    username: element.username,
                    password: element.password
                },
                headers: {
                    "X-CSRFTOKEN": token,
                },
            }).as('response')
        })
        cy.wrap(element.username).as('loggedin_user')
    })
})

And('I login with the same credentials via api', () => {
    cy.get('@username').then((username) => {
        cy.get('@password').then((password) => {
            cy.get("@csrfToken").then((token) => {
                cy.request({
                    method: "POST",
                    url: "/accounts/login/",
                    form: true,
                    body: {
                        username: username,
                        password: password
                    },
                    headers: {
                        "X-CSRFTOKEN": token,
                    },
                }).as('response')
            })
        })
    })
})

Then('The response code should be {int}', (statusCode) => {
    cy.get('@response').then((response) => {
        expect(response.status).to.eq(statusCode)
    })
})

Then('The response code should be {string}', (statusCode) => {
    cy.get('@response').then((response) => {
        expect(response.status).to.eq(parseInt(statusCode))
    })
})

And('Logout via api', () => {
    cy.request({
        method: "GET",
        url: "/accounts/logout/"
    }).as('response')
    cy.get('@response').then((response) => {
        expect(response.body).to.contain('href=\"/accounts/register/\"')
    })
})

And('The response should contain {string}', (reponseContent) => {
    cy.get('@response').then((response) => {
        expect(response.body).to.contain(reponseContent)
    })
})

And('The response should contain redirections {string}', (redirection) => {
    cy.get('@response').then((response) => {
        const redirects = Object.values(response.redirects)
        const expectedRedirect = ['302: ' + Cypress.config().baseUrl + redirection]
        expect(redirects).to.include.members(expectedRedirect)
    })
})

And('The response should contain redirections with params {string} and {string}', (redirection, param) => {
    cy.get('@response').then((response) => {
        cy.get(`${param}`).then((param) => {
            const redirects = Object.values(response.redirects)
            const expectedRedirect = ['302: ' + Cypress.config().baseUrl + redirection + param]
            expect(redirects).to.include.members(expectedRedirect)
        })
    })
})

When('I make a call to the feeds detail', () => {
    cy.get('@feed_id').then((feed_id) => {
        cy.request({
            method: "GET",
            url: "/feeds/" + feed_id
        }).as('response')
    })
})

And('The Date added in the response should match with the timestamp', () => {
    cy.get('@response').then((response) => {
        cy.get('@currentTimestamp').then((currentTimestamp) => {
            expect(response.body).to.contain('Date added')
            const regEx = new RegExp("(?<=Date\\sadded\\s*<\/dt>\\s*.*<dd>).*(?=<\/dd>\\s*<dt>Last\\s*updated)")
            var match = response.body.match(regEx)
            var dateAddedTime = match[0]
            expect(dateAddedTime).to.eq(currentTimestamp.replace('am', 'a.m.').replace('pm', 'p.m.'))
            console.log(dateAddedTime);
        })
    })
})

