And('The userdetails should be stored in db', () => {
    cy.get('@username').then((username) => {
        cy.task("DATABASE", {
            dbConfig: Cypress.env("DB"),
            sql: `
            select * from auth_user where username = '${username}'   
            `
        }).then((result) => {
            console.log(result.rows[0]);
            expect(result.rows[0].username).to.have.string(`${username}`);
        })
    })
})

When('I read a username from db', () => {
    cy.task("DATABASE", {
        dbConfig: Cypress.env("DB"),
        sql: `
        select * from auth_user limit 1   
        `
    }).then((result) => {
        console.log(result.rows[0]);
        cy.wrap(result.rows[0].username).as('username');
    })
})

And('The feed should be stored in db', () => {
    cy.get('@url').then((url) => {
        cy.task("DATABASE", {
            dbConfig: Cypress.env("DB"),
            sql: `
            select count(*) from feed_feed where feed_url = '${url}'   
            `
        }).then((result) => {
            console.log(result.rows[0]);
            expect(result.rows[0].count).to.not.eq('0');
        })
    })
})

And('The feed should not be stored in db', () => {
    cy.get('@url').then((url) => {
        cy.task("DATABASE", {
            dbConfig: Cypress.env("DB"),
            sql: `
            select count(*) from feed_feed where feed_url = '${url}'   
            `
        }).then((result) => {
            console.log(result.rows[0]);
            expect(result.rows[0].count).to.eq('0');
        })
    })
})

And('I fetch the feed\'s id from db', () => {
    cy.get('@url').then((url) => {
        cy.task("DATABASE", {
            dbConfig: Cypress.env("DB"),
            sql: `
            select id from feed_feed where feed_url = '${url}'   
            `
        }).then((result) => {
            console.log(result.rows[0]);
            cy.wrap(result.rows[0].id).as('feed_id');
        })
    })
})

And('I fetch the feed\'s id from db for the {string}', (url) => {
    cy.task("DATABASE", {
        dbConfig: Cypress.env("DB"),
        sql: `
        select id from feed_feed where feed_url = '${url}'   
        `
    }).then((result) => {
        console.log(result.rows[0]);
        cy.wrap(result.rows[0].id).as('feed_id');
    })
})

And('I fetch the feed\'s entry count from db for the {string}', (url) => {
    cy.task("DATABASE", {
        dbConfig: Cypress.env("DB"),
        sql: `
        select count(*) from feed_entry where feed_id in (
            select id from feed_feed where feed_url = '${url}')
        `
    }).then((result) => {
        console.log(result.rows[0]);
        cy.wrap(result.rows[0].count).as('feed_entry_count');
    })
})

And('I fetch the feed\'s first entry id from db for the {string} with {string} items', (url, itemcondition) => {
    cy.task("DATABASE", {
        dbConfig: Cypress.env("DB"),
        sql: `
        select id from feed_entry where feed_id in (
            select id from feed_feed where feed_url = '${url}') limit 1
        `
    }).then((result) => {
        console.log(result.rows[0]);
        if(itemcondition === 'non-zero') {
            expect(result.rows.length).to.not.eq(0)
            cy.wrap(result.rows[0].id).as('feed_entry_id');
        } else if(itemcondition === 'zero') {
            expect(result.rows.length).to.eq(0)
            cy.wrap('0').as('feed_entry_id');
        }
    })
})

When('I fetch the {string} feeds bookmark status for the loggedin user', (url) => {
    cy.get('@loggedin_user').then((loggedin_user) => {
        cy.task("DATABASE", {
            dbConfig: Cypress.env("DB"),
            sql: `
            select count(*) from web_bookmark 
            where 
            user_id in (select id from auth_user where username = '${loggedin_user}')
            and 
            feed_id in (select id from feed_feed where feed_url = '${url}')
            `
        }).then((result) => {
            console.log(result.rows[0]);
            cy.wrap(result.rows[0].count).as('feed_bookmark_status');
        })
    })
})

And('I read an existing feed url from db', () => {
    cy.task("DATABASE", {
        dbConfig: Cypress.env("DB"),
        sql: `
        select feed_url from feed_feed limit 1  
        `
    }).then((result) => {
        console.log(result.rows[0]);
        cy.wrap(result.rows[0].feed_url).as('feed_url');
    })
})