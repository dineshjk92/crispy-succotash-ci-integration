import FeedsDetailListPage from "../../../pages/FeedsDetailListPage"

const feedsDetailListPage = new FeedsDetailListPage()

And('I click on Check Updates button', () => {
    feedsDetailListPage.clickCheckUpdates()
})

Then('The update alert message {string} or {string} should be displayed', () => {
    feedsDetailListPage.getUpdatesAlert().then((ele) => {
        const regEx = new RegExp("Found new updates. Enjoy!|Nothing new yet...", "g");
        expect(ele.text()).to.match(regEx)
        cy.wrap(ele.text()).as('updateMessage')
    })
})

And('Last checked time should not be the same when {string}', (expectedUpdateMessage) => {
    cy.get('@currentTimestamp').then((currentTimestamp) => {
        cy.get('@updateMessage').then((updateMessage)=> {
            if (updateMessage === expectedUpdateMessage) {
                const regEx = new RegExp(".*Last checked.*" + currentTimestamp.replace('am', 'a.m.').replace('pm', 'p.m.'), "g");
                cy.contains(regEx)
            }
        })
    })
})

And('I read the first feed\'s entries count', ()=> {
    feedsDetailListPage.getFeedsEntryCount().then((ele)=> {
        cy.wrap(ele.text()).as('feedsEntryCount')
    })
})

Then('The feed\'s entries for the first feed should not match when {string}', (expectedUpdateMessage) => {
    cy.get('@feedsEntryCount').then((feedsEntryCount) => {
        cy.get('@updateMessage').then((updateMessage)=> {
            if (updateMessage === expectedUpdateMessage) {
                cy.get(feedsDetailListPage.getFeedsEntryCount()).then((ele)=> {
                    expect(ele.text).to.not.eq(feedsEntryCount)
                })
            }
        })
    })
})