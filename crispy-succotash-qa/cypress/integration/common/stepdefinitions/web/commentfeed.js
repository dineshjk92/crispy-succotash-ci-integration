import CommonPage from "../../../pages/CommonPage"
import FeedsDetailPage from "../../../pages/FeedsDetailPage"

const commonPage = new CommonPage()
const feedsDetailPage = new FeedsDetailPage()

And('I read the first feed\'s comment count', ()=> {
    commonPage.getFeedsEntryCount().then((ele) => {
        cy.wrap(ele.text()).as('commentsCount')
    })
})

And('I add a new comment', ()=> {
    feedsDetailPage.addComment('Auto Test Comment')
})

Then('First feed\'s comment count should be incremented', ()=> {
    cy.get('@commentsCount').then((commentsCount)=> {
        commonPage.getFeedsEntryCount().then((ele) => {
            expect(ele.text()).to.not.eq(commentsCount)
        })
    })
})