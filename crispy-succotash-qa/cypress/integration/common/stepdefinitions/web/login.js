const addContext = require("mochawesome/addContext");
import LoginPage from '../../../pages/LoginPage';
import MainPage from "../../../pages/MainPage"

const loginPage = new LoginPage
const mainPage = new MainPage()


When('I click on the login link', () => {
    mainPage.clickLogin()
})

And("I login with credentials", (datatable) => {
  datatable.hashes().forEach((element) => {
    loginPage.login(element.username, element.password);
  })
})
