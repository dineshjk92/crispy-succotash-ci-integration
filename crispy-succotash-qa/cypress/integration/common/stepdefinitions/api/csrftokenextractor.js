
class CSRFTokenExtractor {
    static storeCSRFToken(response) {
        if (response.status !== 500) {
            expect(response.body).to.contain('csrfmiddlewaretoken')
            var match = response.body.match(/name='csrfmiddlewaretoken'.*value='.*'/)
            var csrftoken = match[0].split('value=')[1].replace(/\'/g, '')
            console.log(csrftoken);
            cy.wrap(csrftoken).as('csrfToken')
        }
    }
}

export default CSRFTokenExtractor;