And('I make a call to {string} bookmark to the feed if not added already', (bookmark_action) => {
    cy.get('@feed_id').then((feed_id) => {
        cy.get('@feed_bookmark_status').then((feed_bookmark_status) => {
            if ((bookmark_action === 'add' && feed_bookmark_status == 0) ||
                (bookmark_action === 'remove' && feed_bookmark_status > 0)) {
                cy.get("@csrfToken").then((token) => {
                    cy.request({
                        method: "GET",
                        url: "/feeds/" + feed_id + "/toggle-bookmark/",
                        headers: {
                            "X-CSRFTOKEN": token,
                        },
                        "failOnStatusCode": false
                    }).as('response')
                })
            }
        })
    })
})

And('The response should {string} {string}', (condition, bookmark_class_name) => {
    cy.get('@response').then((response) => {
        if(condition === 'not contain') {
            expect(response.body).to.not.contain(bookmark_class_name)
        } else if(condition === 'contain') {
            expect(response.body).to.contain(bookmark_class_name)
        }
    })

})