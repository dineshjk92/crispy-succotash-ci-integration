//refer commonstepsapi.js from "../common/commonstepsapi.js"
//for more login related api functions

import CSRFTokenExtractor from "./csrftokenextractor"

Given('I request signup page', () => {
    cy.request({
        method: "GET",
        url: "/accounts/register/"
    }).as('response')
    cy.get('@response').then((response) => {
        CSRFTokenExtractor.storeCSRFToken(response)
    })
})

When('I Signup with auto-generated {string} and {string}', (username, password) => {
    const randomUsername = username + Math.floor((Math.random() * 10000) + 100)
    cy.wrap(randomUsername).as('username')
    cy.wrap(password).as('password')
    cy.get("@csrfToken").then((token) => {
        cy.request({
            method: "POST",
            url: "/accounts/register/",
            form: true,
            body: {
                username: randomUsername,
                password1: password,
                password2: password
            },
            headers: {
                "X-CSRFTOKEN": token,
            },
        }).as('response')
    })
})

When('I Signup with the existing username', () => {
    cy.get('@username').then((username) => {
        cy.get("@csrfToken").then((token) => {
            cy.request({
                method: "POST",
                url: "/accounts/register/",
                form: true,
                body: {
                    username: username,
                    password1: "pass1234",
                    password2: "pass1234"
                },
                headers: {
                    "X-CSRFTOKEN": token,
                },
            }).as('response')
        })
    })
})

