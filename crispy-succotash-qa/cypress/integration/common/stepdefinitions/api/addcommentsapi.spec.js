import CSRFTokenExtractor from "./csrftokenextractor"

When('I make a call to the feed\'s first entry', () => {
    cy.get('@feed_entry_id').then((feed_entry_id) => {
        cy.get("@csrfToken").then((token) => {
            cy.request({
                method: "GET",
                url: "/feeds/" + feed_entry_id + "/entry/",
                headers: {
                    "X-CSRFTOKEN": token,
                },
                failOnStatusCode: false
            }).as('response')
        })
    })
    cy.get('@response').then((response) => {
        CSRFTokenExtractor.storeCSRFToken(response)
    })
})

When('I add a comment to the feed entry', () => {
    var comment = 'Auto Test Comment'
    cy.wrap(comment).as('comment')
    cy.get('@feed_entry_id').then((feed_entry_id) => {
        cy.get("@csrfToken").then((token) => {
            cy.request({
                method: "POST",
                url: "/feeds/" + feed_entry_id + "/entry/",
                form: true,
                body: {
                    content: comment,
                    entry: feed_entry_id
                },
                headers: {
                    "X-CSRFTOKEN": token,
                },
                "failOnStatusCode": false
            }).as('response')
        })
    })

})

And('The response should contain the username and comment message', () => {
    cy.get('@response').then((response) => {
        cy.get('@loggedin_user').then((loggedin_user) => {
            cy.get('@comment').then((comment) => {
                expect(response.body).to.contain(loggedin_user)
                expect(response.body).to.contain(comment)
            })
        })
    })

})