//refer commonstepsapi.js from "../common/commonstepsapi.js"
//for more login related api functions

import CSRFTokenExtractor from "./csrftokenextractor"

Given('I request login page', () => {
    cy.request({
        method: "GET",
        url: "/accounts/login/"
    }).as('response')
    cy.get('@response').then((response) => {
        CSRFTokenExtractor.storeCSRFToken(response)
    })
})